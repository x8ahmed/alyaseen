<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class SharedProduct extends Model
{
    //
    protected $table='shared_product';

    public function getUser(){
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function getProduct(){
        return $this->belongsTo(Products::class,'product_id','id');
    }
}
