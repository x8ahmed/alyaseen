<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    //
    protected $table='employees';
    public function getUser(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function getEmployee(){
        return $this->hasMany(MembersManagement::class,'employee_id');

    }

}
