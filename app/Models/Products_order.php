<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Products_order extends Model
{
    //

    protected $table="products_order";
    protected $fillable=['name','phone','area','email','information_additional','status','location','long','lat','order_id','attach'];
public function getOrder(){
    return $this->belongsTo(Orders::class,'order_id','id');
}

}
