<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_attribute extends Model
{
    //
    protected $table='products_attribute';
    public function product()
    {
        return $this->belongsTo(Products::class,'product_id');
    }
    public function attribute()
    {
        return $this->belongsTo(Attribute::class,'attribute_id');
    }
}
