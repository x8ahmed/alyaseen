<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    //
    protected $fillable = ['id', 'group_id', 'name','en_name', 'category_id', 'value'];
    protected $table = 'attributes';

    public function attributes ()
    {
        return $this->hasMany(Attribute::class, 'group_id');
    }

    public function group ()
    {
        return $this->belongsTo(Attribute::class, 'group_id');
    }

    public function category ()
    {
        return $this->belongsTo(Category::class);
    }

    public function products ()
    {
        return $this->belongsToMany(Products::class, 'products_attribute', 'attribute_id', 'product_id')
            ->withPivot('attribute_value','attribute_value_en');
    }
}
