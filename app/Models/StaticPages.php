<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaticPages extends Model
{
    //
    protected $table='staticpage';
    public function getImages(){
        return $this->hasMany(Images::class,'staticPage_id');
    }
}
