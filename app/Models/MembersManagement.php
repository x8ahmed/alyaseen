<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class MembersManagement extends Model
{
    //

    protected $table='members_management';

    public function getEmployee(){
        return $this->belongsTo(Employees::class,'employee_id','id');
    }
}
