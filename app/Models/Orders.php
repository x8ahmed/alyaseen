<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Orders extends Model
{
    //
    protected $table='orders';
    public function getProduct(){
        return $this->belongsTo(Products::class,'product_id','id');
    }
    public function getUser(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function getProductOrder(){
        return $this->hasMany(Products_order::class,'order_id');
    }

    public function getSumOrderProduct()
    {
        $priceProduct=$this->getProduct->price;
        $sum=$this->qty*$priceProduct;
        if($sum==0)
            return 0;
        return $sum;
    }
}
