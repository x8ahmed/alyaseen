<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products_publication extends Model
{
    protected $table='products_publication';
    protected $fillable=['attachment','product_id'];
    //
    public function product(){
        return $this->belongsTo(Products::class,'product_id','id');
    }
}
