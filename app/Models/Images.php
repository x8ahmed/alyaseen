<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    //
    protected $table='images';

    public function products(){
        return $this->belongsTo(Products::class,'product_id','id');
    }

    public function getNews(){
        return $this->belongsTo(News::class,'news_id','id');

    }

    public function getStaticPage(){
        return $this->belongsTo(StaticPages::class,'staticPage_id','id');
    }
}
