<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class products_permissions extends Model
{
    //
    protected $table='products_permissions';

public function getuser(){
    return $this->belongsTo(User::class,'user_id','id');

}

public function getproduct(){
    return $this->belongsTo(Products::class,'product_id','id');

}

public function getpermission(){
    return $this->belongsTo(Permissions::class,'permission_id','id');
}
}
