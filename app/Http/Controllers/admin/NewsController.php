<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Images;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    //

    public function index(){
$newss=News::all();
        return view('admin.news.index',compact('newss'));
    }

    public function create(){
        return view('admin.news.news');
    }

    public function store(Request $request){
        $this->validate($request, [
            'title' => 'required',
            'en_title' => 'max:100|min:3',
            'descr' => 'required',
            'en_descr' => 'required',
            'link' => 'required',

        ]);

        $news=new News();
        $news->title=$request->title;
        $news->en_title=$request->en_title;
        $news->descr=$request->descr;
        $news->en_descr=$request->en_descr;
        $news->slider=isset($request->slider)?1:0;
        $news->last_news=isset($request->last_news)?1:0;
        $news->link=$request->link;
        $news->title_image=$request->title_image;
        $news->titleen_image=$request->titleen_image;

        $file = $request->file('image');
        if ($request->hasFile('image')) {
                $fileName = 'image_news-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
                $destinationPath = 'uploads';
                $request->file('image')->move($destinationPath, $fileName);
                $news->image = $fileName;
        }
        $news->save();





        return redirect('/news')
            ->with('success', 'تم انشاء الخبر بنجاح');
    }


    public function edit($name){
        $news=News::where('title',$name)->first();

        return view('admin.news.news',compact('news'));
    }

    public function update(Request $request ,$id){
        $this->validate($request, [
            'title' => '',
            'en_title' => 'max:100|min:3',
            'descr' => '',
            'en_descr' => '',
            'link' => '',

        ]);

        $news=News::find($id);
        $news->title=$request->title;
        $news->en_title=$request->en_title;
        $news->descr=$request->descr;
        $news->en_descr=$request->en_descr;
        $news->slider=isset($request->slider)?1:0;
        $news->last_news=isset($request->last_news)?1:0;
        $news->link=$request->link;
        $news->title_image=$request->title_image;
        $news->titleen_image=$request->titleen_image;

        $file = $request->file('image');
        if ($request->hasFile('image')) {
            $old_file = 'uploads/' . $news->image;
            if (is_file($old_file)) unlink($old_file);
            $fileName = 'image_news-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $destinationPath = 'uploads';
            $request->file('image')->move($destinationPath, $fileName);
            $news->image = $fileName;
        }

        $news->save();


//                $data = ['title_image' => $request->title_image, 'titleen_image' => $request->titleen_image];
//                $title_image = array_values($data['title_image']);
//                $titleen_image = array_values($data['titleen_image']);




                     return redirect('/news')
            ->with('success', 'تم تعديل الخبر بنجاح');
    }

    public function destroy($id){
        $news=News::find($id);

        $news->delete();
        return response()->json(['success' => 'true']);

    }
}
