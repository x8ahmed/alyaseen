<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    //
    public function index(){
        $users=User::all()->where('id','!=',2);
        return view('admin.users.index',compact('users'));
    }

    public function create(){
        return view('admin.users.user');
    }

    public function store(Request $request){
        $this->validate($request,[
            'username' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        User::create([
            'username' =>$request->username,
            'email' => $request->email,
            'role'=>'c',
            'password' => Hash::make($request->password),
        ]);
        return redirect('/users')->with('success', 'تم انشاء المستخدم بنجاح .');
    }

    public function edit($name){
        $user=User::where('username',$name)->first();
        return view('admin.users.user',compact('user'));

    }

    public function update(Request $request,$id){
        $user=User::find($id);
        $user->username=$request->username;
            $user->email=$request->email;
        $user->password= bcrypt($request->password);
$user->save();

        return redirect('/users')->with('success', 'تم تعديل المستخدم بنجاح .');

    }

    public function destroy($id){
        $user=User::find($id);
        $user->delete();
        return response()->json(['success' => 'true']);

    }

    public function admin_user($user_id){
        $user_role = User::find($user_id);
        if ($user_role->hasRole('admin')){
            $user_role->roles()->detach();
            $c=Role::where('name','customer')->first();
            $user_role->roles()->attach($c->id); // id only
            $user_role->role = 'c';
            $user_role->save();
            return redirect()->back()->with('success', 'تم تعيين المستخدم كعضو عادي بنجاح .');

        }else {
            $user_role->roles()->detach();
            $admin = Role::where('name', 'admin')->first();
            $user_role->roles()->attach($admin->id); // id only
            $user_role->role = 'admin';
            $user_role->save();
            return redirect()->back()->with('success', 'تم تعيين المستخدم كمدير بنجاح .');

        }
    }

    public function block_user($user_id){
        $user_role = User::find($user_id);
        if ($user_role->hasRole('block')){
            $user_role->roles()->detach();
            $c=Role::where('name','customer')->first();
            $user_role->roles()->attach($c->id); // id only
            $user_role->role = 'c';
            $user_role->save();
            return redirect()->back()->with('success', 'تم ازاله حظر المستخدم  بنجاح .');

        }else {
            $user_role->roles()->detach();
            $block = Role::where('name', 'block')->first();
            $user_role->roles()->attach($block->id); // id only
            $user_role->role = 'block';
            $user_role->save();
            return redirect()->back()->with('success', 'تم حظر المستخدم بنجاح .');

        }
    }

    public function admins(){
        $admins=User::where('id','!=',Auth::user()->id)->where('role','admin')->get();
        return view('admin.users.admins',compact('admins'));
    }

    public function blocked(){
        $blocks=User::where('role','block')->get();
        return view('admin.users.blocked',compact('blocks'));
    }
}
