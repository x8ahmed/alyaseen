<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Systems;
use App\Role;
use Illuminate\Http\Request;

class SystemsController extends Controller
{
    //
    public function index(){
        $systems=Systems::all();
        $role_s=Role::where('name','suppliers')->first();
        $role_e=Role::where('name','employee')->first();
        return view('admin.systems.index',compact('systems','role_e','role_s'));
    }

    public function create(){
        $roles=Role::where('name','!=','admin')->where('name','!=','visitor')->where('name','!=','block')->where('name','!=','customer')->get();
        return view('admin.systems.system',compact('roles'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'en_name' => 'required|max:100|min:3',
            'link' => 'required|max:100|min:3',

        ]);

        $system=new Systems();
        $system->name=$request->name;
        $system->en_name=$request->en_name;
        $system->link=$request->link;
        $system->role_id=$request->role_id;
        $file = $request->file('image');
        if ($request->hasFile('image')) {
            $fileName = 'system-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $destinationPath = 'uploads';
            $request->file('image')->move($destinationPath, $fileName);
            $system->image = $fileName;
        }
        $system->save();
        return redirect('/systems')->with('success', 'تم انشاء النظام بنجاح');

    }

    public function edit($name){
$system=Systems::where('name',$name)->first();
        $roles=Role::where('name','!=','admin')->where('name','!=','visitor')->where('name','!=','block')->where('name','!=','customer')->get();

return view('admin.systems.system',compact('system','roles'));
    }

    public function update(Request $request,$id){
       $system=Systems::find($id);
       $system->name=$request->name;
       $system->en_name=$request->en_name;
       $system->link=$request->link;
       $system->role_id=$request->role_id;
        $file = $request->file('image');
        if ($request->hasFile('image')) {
            $old_file = 'uploads/' . $system->image;
            if (is_file($old_file)) unlink($old_file);
            $fileName = 'supplier-' . time() . '-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $destinationPath = 'uploads';
            $request->file('image')->move($destinationPath, $fileName);
            $system->image = $fileName;
        }
        $system->save();
return redirect('/systems')
    ->with('success', 'تم تعديل النظام بنجاح');
    }

    public function destroy($id)
    {
        $system = Systems::find($id);
        $system->delete();
        return response()->json(['success' => 'true']);
    }
}
